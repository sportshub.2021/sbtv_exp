[
       {
                "name": "SPORTS",
                "samples": [


       {
          "name": "ASSP1",
          "logo_uri": "https://divign0fdw3sv.cloudfront.net/Images/ChannelLogo/contenthub/154_144.png",
          "uri": "https://tv.superbox1717.com/script/main.php/manifest.mpd?ch=https://d3j4fjrwclc3o8.cloudfront.net/CH1/masterCH1.mpd",
          "drm_scheme": "clearkey", 
          "drm_license_url": "https://elementotv-zoom-adr.koyeb.app/clearkey/results.php?keyid=b3ccf73a7a664f85ae7411367ffb2fa7&key=9687d91b7fd647a88183d0a4cc925f77"
       },

       {
          "name": "ASSP2",
          "logo_uri": "https://divign0fdw3sv.cloudfront.net/Images/ChannelLogo/contenthub/138_144.png",
          "uri": "https://tv.superbox1717.com/script/main.php/manifest.mpd?ch=https://d3j4fjrwclc3o8.cloudfront.net/CH4/masterCH4.mpd",
          "drm_scheme": "clearkey", 
          "drm_license_url": "https://elementotv-zoom-adr.koyeb.app/clearkey/results.php?keyid=8f1da399325247eca6acdd7bafb95fa2&key=41f234fb88590af3b0d224111a61bf71"
       },
   
       {
          "name": "ASSP3",
          "logo_uri": "https://divign0fdw3sv.cloudfront.net/Images/ChannelLogo/contenthub/164_144.png",
          "uri": "https://tv.superbox1717.com/script/main.php/manifest.mpd?ch=https://d3j4fjrwclc3o8.cloudfront.net/CH5/masterCH5.mpd",
          "drm_scheme": "clearkey", 
          "drm_license_url": "https://elementotv-zoom-adr.koyeb.app/clearkey/results.php?keyid=0d6d2a1ac8d14e19b17455650770cd99&key=ff65174f8ec5d4c88e8d8a21968563cf"
	   },

      {
        "name": "ASSP 4",
        "logo_uri": "https://divign0fdw3sv.cloudfront.net/Images/ChannelLogo/contenthub/241_144.png",
        "uri": "https://linearjitp02-playback.astro.com.my/dash-wv/linear/2506/manifest.mpd",
        "drm_scheme": "clearkey", 
        "drm_license_url": "https://cloudtv-playback.stackstaging.com/astrogo.com.my/live/results.php?keyid=79f4028730acca9ab8b00f26158ddb10&key=91febe843c08c7cc523efd827292e40e"
      },

      {
        "name": "W-SPORT",
        "logo_uri": "https://divign0fdw3sv.cloudfront.net/Images/ChannelLogo/contenthub/503_144.png",
        "uri": "https://linearjitp02-playback.astro.com.my/dash-wv/linear/5060/default_primary.mpd",
        "drm_scheme": "clearkey", 
        "drm_license_url": "https://cloudtv-playback.stackstaging.com/astrogo.com.my/live/results.php?keyid=010cbb9cf1a01ead38070730d38bc710&key=9cd5c0d1e54c22e035d88dd3c1dfc84c"
      },

      {
         "name": "ASSP+ 1",
         "logo_uri": "https://i.postimg.cc/8P44Lb1k/aotgpl.jpg",
         "uri": "https://linearjitp02-playback.astro.com.my/dash-wv/linear/9986/default.mpd",
          "drm_scheme": "clearkey", 
          "drm_license_url": "https://cloudtv-playback.stackstaging.com/astrogo.com.my/live/results.php?keyid=cb18945925e65d411fbb986d72531010&key=15ff74ae1064c5f2fb00d2995fa88955" 
	    },

      {
         "name": "ASSP+ 2",
         "logo_uri": "https://i.postimg.cc/8P44Lb1k/aotgpl.jpg",
         "uri": "https://linearjitp02-playback.astro.com.my/dash-wv/linear/9987/default_primary.mpd",
          "drm_scheme": "clearkey", 
          "drm_license_url": "https://cloudtv-playback.stackstaging.com/astrogo.com.my/live/results.php?keyid=8d580c263ffde691b4249dfcac43fd10&key=f3148d1e737dece3554b889868831659" 
	    },

      {
         "name": "ASSP+ 3",
         "logo_uri": "https://i.postimg.cc/8P44Lb1k/aotgpl.jpg",
         "uri": "https://linearjitp02-playback.astro.com.my/dash-wv/linear/9988/default_primary.mpd",
          "drm_scheme": "clearkey", 
          "drm_license_url": "https://cloudtv-playback.stackstaging.com/astrogo.com.my/live/results.php?keyid=8ab014de03c31a84a835583c348a5e10&key=5ed727c3f28bac59aa3e5914f5ad455c" 
	    },

      {
         "name": "ASSP+ 4",
         "logo_uri": "https://i.postimg.cc/8P44Lb1k/aotgpl.jpg",
         "uri": "https://linearjitp02-playback.astro.com.my/dash-wv/linear/9989/default_primary.mpd",
          "drm_scheme": "clearkey", 
          "drm_license_url": "https://cloudtv-playback.stackstaging.com/astrogo.com.my/live/results.php?keyid=de761cd285aa2b1c1620d16ddbade010&key=f12de5f0de92c52f841705d9d6449d3a" 
	    },
      

      {
        "name": "Bein Sports",
        "logo_uri": "https://linear-poster.astro.com.my/prod/logo/beIN_Sports_v1.png",
        "uri": "https://linearjitp02-playback.astro.com.my/dash-wv/linear/408/default.mpd",
          "drm_scheme": "clearkey", 
          "drm_license_url": "https://cloudtv-playback.stackstaging.com/astrogo.com.my/live/results.php?keyid=1a655189ab5c49eb235308c2b1a9e710&key=3c4508af348844107f5e026a4fd6b16e" 
      },

      {
        "name": "Bein Sports 2",
        "logo_uri": "https://linear-poster.astro.com.my/prod/logo/beINSPORTS2.png",
        "uri": "https://linearjitp02-playback.astro.com.my/dash-wv/linear/5066/default.mpd",
          "drm_scheme": "clearkey", 
          "drm_license_url": "https://cloudtv-playback.stackstaging.com/astrogo.com.my/live/results.php?keyid=39c175581e237eff9559607eb9b23c10&key=5102b12aac7756c65dcb46a101d960d3" 
      },

      {
        "name": "Bein Sports 3",
        "logo_uri": "https://linear-poster.astro.com.my/prod/logo/beIN_SPORTS3_V2.png",
        "uri": "https://linearjitp02-playback.astro.com.my/dash-wv/linear/2705/default.mpd",
          "drm_scheme": "clearkey", 
          "drm_license_url": "https://cloudtv-playback.stackstaging.com/astrogo.com.my/live/results.php?keyid=20c884ef8ed26b2762f8b1a78f2d1910&key=042a21bf236ca729b1e343ed6e0a6337" 
      },

      {
        "name": "SPOTV",
        "logo_uri": "https://i.postimg.cc/PJbsysVz/1634257286-thumb-spotv.png",
        "uri": "https://linearjitp02-playback.astro.com.my/dash-wv/linear/5058/default.mpd",
        "drm_scheme": "clearkey", 
        "drm_license_url": "https://cloudtv-playback.stackstaging.com/astrogo.com.my/live/results.php?keyid=c0e1804aa1d9fd9c41c41bf0f61a5f10&key=758823e4eabb6e4c8c036d073db46b8c" 
      },

      {
        "name": "SPOTV 2",
        "logo_uri": "https://i.postimg.cc/ydZzJSFG/1634257305-thumb-spotv-2.png",
        "uri": "https://linearjitp02-playback.astro.com.my/dash-wv/linear/5079/default_primary.mpd",
        "drm_scheme": "clearkey", 
        "drm_license_url": "https://cloudtv-playback.stackstaging.com/astrogo.com.my/live/results.php?keyid=5efd26da5001363b4d6fa4a9c812ad10&key=ed6d67d953d14b026b2602cf8846577e" 
      },

      {
        "name": "Unifi Sports",
        "logo_uri": "https://playtv.unifi.com.my:7044/CPS/images/universal/film/logo/202304/20230418/202304180833546499vy.png",
        "uri": "https://unifi-live11.secureswiftcontent.com/UnifiHD/live11.mpd"
      },

 
      {
        "name": "WWE Network",
        "logo_uri": "https://i.postimg.cc/DwxRLwcg/WWE-Network-logo.png",
        "uri": "https://linearjitp02-playback.astro.com.my/dash-wv/linear/2603/default.mpd",
        "drm_scheme": "clearkey", 
        "drm_license_url": "https://cloudtv-playback.stackstaging.com/astrogo.com.my/live/results.php?keyid=0cbc4d3b4fbd9af512acb2488bb42910&key=30528c4ef882954e5707cd1001d66121"
      }

    ]
  }
  
          ] 
        }
      ]
